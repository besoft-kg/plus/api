<?php

namespace App\Http\Controllers\V1;

use App\Models\Session;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Firebase\Auth\Token\Exception\InvalidToken;

class AuthController extends Controller
{
  public function post_index(Request $request)
  {
    $this->validate($request, [
      'id_token' => 'bail|required|string',
      'platform' => 'bail|required|string|in:android,ios,web',
      'language' => 'bail|required|string|in:ru,ky',
      'version_code' => 'bail|required|integer',
      'fcm_token' => 'bail|string',
    ]);

    $auth = app('firebase.auth');

    try {
      $verifiedIdToken = $auth->verifyIdToken($request->get('id_token'));
    } catch (InvalidToken $e) {
      return response([
        'status' => 'unauthorized',
        'result' => -1,
        'payload' => [
          'message' => 'The token is invalid: ' . $e->getMessage(),
        ],
      ], 401);
    } catch (\InvalidArgumentException $e) {
      return response([
        'status' => 'unauthorized',
        'result' => -1,
        'payload' => [
          'message' => 'The token could not be parsed: ' . $e->getMessage(),
        ],
      ], 403);
    }

    $sub = $verifiedIdToken->claims()->get('sub');

    $firebase_user = $auth->getUser($sub);
    $user = User::where('provider', 'phone_number')->where('provider_id', substr($firebase_user->phoneNumber, 1))->first();

    if (!$user) {
      $user = new User();
      $user->provider = 'phone_number';
      $user->provider_id = substr($firebase_user->phoneNumber, 1);
      $user->provider_data = $firebase_user;
      $user->language = $request->get('language');
    }

    $user->last_action = Carbon::now();
    $user->save();

    Session::updateOrCreate([
      'user_id' => $user->id,
      'key' => $sub,
      'user_agent' => $request->header('User-Agent'),
      'platform' => $request->get('platform'),
    ], [
      'last_action' => Carbon::now(),
      'version_code' => $request->get('version_code'),
      'fcm_token' => $request->has('fcm_token') ? $request->get('fcm_token') : null,
    ]);

    return [
      'status' => 'success',
      'result' => 0,
      'payload' => [
        'user' => $user->makeVisible([
          'provider',
          'provider_id',
          'provider_data',
          'created_at',
          'email',
          'updated_at',
          'language',
          'location_last_refresh',
          'location_lat',
          'location_lng',
          'settings',
        ]),
        'is_new' => $user->is_new,
      ],
    ];
  }
}
