<?php

namespace App\Http\Controllers\V1;

use App\Http\Utils\Helper;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Like;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function post_index(Request $request)
    {
        $this->validate($request, [
            'content' => 'bail|required|string',
            'location_lat' => 'bail|required|numeric',
            'location_lng' => 'bail|required|numeric',
        ]);

        $user = Auth::user();
        $user->location_lat = $request->get('location_lat');
        $user->location_lng = $request->get('location_lng');
        $user->location_last_refresh = Carbon::now();
        $user->save();

        $item = new Post();
        $item->content = $request->get('content');
        $item->location_lat = $request->get('location_lat');
        $item->location_lng = $request->get('location_lng');
        $item->save();

        return [
            'status' => 'success',
            'result' => 0,
            'payload' => Post::with('user')->find($item->id),
        ];
    }

    public function post_like(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'bail|required|integer|exists:posts,id',
        ]);

        $post = Post::findOrFail($request->get('post_id'));
        $like = Like::where('user_id', Auth::id())->where('post_id', $request->get('post_id'))->first();
        if ($like) {
            $like->delete();
        } else {
            Like::create(['post_id' => $request->get('post_id'), 'user_id' => Auth::id()]);
            if ($post->user_id !== Auth::id()) {
                Helper::sendPushNotification($post->user_id, 'user_likes_post', [
                    'user_login' => Auth::user()->login,
                    'post_id' => $post->id,
                ]);
            }
        }

        return [
            'status' => 'success',
            'result' => 0,
            'payload' => ['liked' => $like ? false : true],
        ];
    }

    public function post_comment(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'bail|required|integer|exists:posts,id',
            'content' => 'bail|string',
        ]);

        $post = Post::findOrFail($request->get('post_id'));

        $item = new Comment();
        $item->post_id = $post->id;
        $item->content = $request->get('content');
        $item->save();

        if ($post->user_id !== Auth::id()) {
            Helper::sendPushNotification($post->user_id, 'user_comments_post', [
                'post_id' => $post->id,
                'user_login' => Auth::user()->login,
            ]);
        }

        return [
            'status' => 'success',
            'result' => 0,
            'payload' => $item,
        ];
    }

    public function get_index(Request $request)
    {
        $this->validate($request, [
            'id' => 'bail|integer',
            'user_id' => 'bail|integer',
            'distance' => 'bail|integer',
            'location_lat' => 'bail|numeric',
            'location_lng' => 'bail|numeric',

            '_page' => 'bail|integer|nullable',
        ]);

        if (!$request->has('id') && $request->has('location_lat') && $request->has('location_lng')) {
            $user = Auth::user();
            $user->location_lat = $request->get('location_lat');
            $user->location_lng = $request->get('location_lng');
            $user->location_last_refresh = Carbon::now();
            $user->save();
        }

        if ($request->has('id')) {
            $item = Post::with(['user', 'comments'])->findOrFail($request->get('id'));
            $item->views++;
            $item->save();
            return [
                'status' => 'success',
                'result' => 0,
                'payload' => $item,
            ];
        }

        if ($request->has('distance') && $request->get('distance') > 0 && $request->has('location_lat') && $request->has('location_lng')) {
            $items = Post::with('user')->having('distance', '<=', $request->get('distance'));
        } else {
            $items = Post::with('user');
        }

        if ($request->has('user_id')) {
            if (!User::exists($request->get('user_id'))) {
                return [
                    'status' => 'user_not_found',
                    'result' => -1,
                    'payload' => null,
                ];
            }
            $items = $items->where('user_id', $request->get('user_id'));
        }

        $_page = $request->has('_page') ? $request->get('_page') : 1;

        $items = $items->orderBy('created_at', 'desc')->offset(($_page - 1) * 15)->limit(15)->get()->toArray();
        Post::whereIn('id', array_map(function($item){
            return $item['id'];
        }, $items))->update(['views' => DB::raw('views + 1')]);

        return [
            'status' => 'success',
            'result' => 0,
            'payload' => $items,
        ];
    }
}
