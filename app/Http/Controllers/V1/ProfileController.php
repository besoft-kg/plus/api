<?php

namespace App\Http\Controllers\V1;

use App\Models\Image;
use App\Models\Post;
use App\Models\Like;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function post_index(Request $request)
    {
        $this->validate($request, [
            'login' => 'bail|string|regex:/^[a-z0-9\_]+$/|max:50',
            'full_name' => 'bail|string|max:250',
            'language' => 'bail|string|in:ru,ky',
        ]);

        $user = Auth::user();

        if ($request->has('login') && $request->get('login') !== $user->login) {
            if (User::where('login', $request->get('login'))->count() > 0) {
                return response([
                    'status' => 'login_is_taken',
                    'result' => -1,
                ], 422);
            }
            $user->login = $request->get('login');
        }

        if ($request->has('full_name')) $user->full_name = $request->get('full_name');
        if ($request->has('language')) $user->language = $request->get('language');

        $user->save();

        return [
            'status' => 'success',
            'result' => 0,
            'payload' => [
                'user' => $user->makeVisible([
                    'provider',
                    'provider_id',
                    'provider_data',
                    'created_at',
                    'email',
                    'updated_at',
                    'language',
                    'location_last_refresh',
                    'location_lat',
                    'location_lng',
                    'settings',
                ]),
            ],
        ];
    }

    public function get_index(Request $request)
    {
        return [
            'status' => 'success',
            'result' => 0,
            'payload' => User::find(Auth::id())->makeVisible([
                'provider',
                'provider_id',
                'provider_data',
                'created_at',
                'email',
                'updated_at',
                'language',
                'location_last_refresh',
                'location_lat',
                'location_lng',
                'settings',
            ]),
        ];
    }

    public function post_picture(Request $request)
    {
        $this->validate($request, [
            'picture' => 'bail|image',
        ]);

        $user = Auth::user();

        if ($request->has('picture')) {
            $image = new Image();
            $image->attachFile($request->file('picture'), 'picture');
            $image->save();

            $old_picture_id = $user->picture_id;

            $user->picture_id = $image->id;
            $user->save();

            if ($old_picture_id !== null) {
                Image::destroy($old_picture_id);
            }
        } elseif ($user->picture_id !== null) {
            $picture_id = $user->picture_id;
            $user->picture_id = null;
            $user->save();
            Image::destroy($picture_id);
        }

        return [
            'status' => 'success',
            'result' => 0,
            'payload' => User::find(Auth::id())->makeVisible([
                'provider',
                'provider_id',
                'provider_data',
                'created_at',
                'email',
                'updated_at',
                'language',
                'location_last_refresh',
                'location_lat',
                'location_lng',
                'settings',
            ]),
        ];
    }

    public function get_like(Request $request)
    {
        return [
            'status' => 'success',
            'result' => 0,
            'payload' => Like::with('post')
                ->whereNull('comment_id')
                ->where('user_id', Auth::id())
                ->orderBy('created_at', 'desc')
                ->limit(15)
                ->get(),
        ];
    }

    public function get_post(Request $request)
    {
        return [
            'status' => 'success',
            'result' => 0,
            'payload' => Post::where('user_id', Auth::id())->orderBy('created_at', 'desc')->limit(15)->get(),
        ];
    }
}
