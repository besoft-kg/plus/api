<?php

namespace App\Http\Utils;

use App\Models\Session;
use Illuminate\Support\Facades\Log;

class Helper {
  public static function sendPushNotification($user_id, $type, $payload = []) {
    $sessions = Session::whereNotNull('fcm_token')->where('user_id', $user_id)->get()->toArray();

    $response = Helper::sendFcmRequest([
      'registration_ids' => array_map(function($item){
        return $item['fcm_token'];
      }, $sessions),
      // 'notification' => [
      //   'body' => $message,
      //   'title' => $title,
      //   'sound' => 'default',
      //   'priority' => 'high',
      // ],
      'data' => [
        'type' => $type,
        'payload' => json_encode($payload),
      ],
      // 'android' => [
      //   'default_vibrate_timings' => true,
      //   'default_light_settings' => true,
      //   'default_sound' => true,
      // ],
    ]);

    Log::info($response);
  }

  public static function sendFcmRequest($fields = []) {
    $headers = [
      'Authorization: Bearer ' . env('FCM_API_KEY'),
      'Content-Type: application/json'
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
}
