<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    protected $fillable = [
        'user_id',
        'post_id',
        'comment_id',
        'content',
    ];

    protected $guarded = [
        'location_lat',
        'location_lng',
    ];

    protected $hidden = [
        'location_lat',
        'location_lng',
    ];

    protected static function booted()
    {
        static::addGlobalScope(function ($builder) {
            $user = Auth::user();

            $builder->selectRaw(DB::raw(
                '*,' .
                '(
                    6371 * acos (
                      cos ( radians(?) )
                        * cos( radians( location_lat ) )
                        * cos( radians( location_lng ) - radians(?) )
                        + sin ( radians(?) )
                        * sin( radians( location_lat ) )
                    )
                  ) AS distance,' .
                '(SELECT COUNT(id) FROM tabekg_likes WHERE tabekg_likes.comment_id = tabekg_comments.id) as likes_count,' .
                '(SELECT COUNT(id) > 0 FROM tabekg_likes WHERE tabekg_likes.comment_id = tabekg_comments.id AND tabekg_likes.user_id = ?) as liked'
            ), [$user->location_lat, $user->location_lng, $user->location_lat, $user->id]);
        });

        static::creating(function ($item) {
            $item->user_id = Auth::user()->id;
        });
    }

    public function comment(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Comment::class, 'id', 'comment_id')->with('user');
    }

    public function post(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Post::class, 'id', 'post_id')->with('user');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
