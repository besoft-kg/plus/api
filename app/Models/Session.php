<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $casts = [
        'last_action' => 'datetime',
    ];

    protected $fillable = [
        'user_id',
        'key',
        'user_agent',
        'last_action',
        'platform',
        'version_code',
        'fcm_token',
    ];
}
