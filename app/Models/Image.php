<?php

namespace App\Models;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class Image extends BaseModel
{
    protected $table = 'images';

    public $file = null;

    protected $appends = ['url'];

    protected $fillable = [
        'name', 'hash',
        'path', 'size',
        'dimensions', 'extension',
    ];

    public function attachFile(UploadedFile $file, $filename)
    {
        if (!$file->isValid()) throw new \Exception('File is invalid!');
        $this->extension = $file->extension();
        $this->name = $file->getClientOriginalName();
        $this->size = ['original' => $file->getSize()];
        $this->path = ['original' => $file->path()];
        $this->hash = Str::random(40);

        $dimensions = getimagesize($_FILES[$filename]['tmp_name']);

        $this->dimensions = ['original' => [
            'width' => $dimensions[0],
            'height' => $dimensions[1],
        ]];

        $this->file = $file;
    }

    public function storeFile()
    {
        $path = [
            'original' => 'uploads/images/' . date('Y/m') . '/original',
        ];

        $this->file->move($path['original'], $this->hash . '.' . $this->extension);

        $this->path = [
            'original' => $path['original'] . '/' . $this->hash . '.' . $this->extension,
        ];
    }

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];

    public function getUrlAttribute()
    {
        return [
            'original' => URL::asset($this->path['original']),
        ];
    }

    protected $casts = [
        'path' => 'array',
        'size' => 'array',
        'dimensions' => 'array',
    ];

    protected static function booted()
    {
        static::creating(function ($item) {
            //$item->user_id = Auth::user()->id;
            $item->storeFile();
        });

        static::deleting(function ($item) {
            unlink($item->path['original']);
        });
    }
}
