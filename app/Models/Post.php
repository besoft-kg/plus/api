<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    protected $hidden = [
        'location_lat',
        'location_lng',
    ];

    protected $fillable = [
        'views',
    ];

    protected static function booted()
    {
        static::addGlobalScope(function ($builder) {
            $user = Auth::user();

            $builder->selectRaw(DB::raw(
                '*,' .
                        '(
                    6371 * acos (
                      cos ( radians(?) )
                        * cos( radians( location_lat ) )
                        * cos( radians( location_lng ) - radians(?) )
                        + sin ( radians(?) )
                        * sin( radians( location_lat ) )
                    )
                  ) AS distance,' .
                '(SELECT COUNT(id) FROM tabekg_likes WHERE tabekg_likes.post_id = tabekg_posts.id) as likes_count,' .
                '(SELECT COUNT(id) FROM tabekg_comments WHERE tabekg_comments.post_id = tabekg_posts.id) as comments_count,' .
                '(SELECT COUNT(id) > 0 FROM tabekg_likes WHERE tabekg_likes.post_id = tabekg_posts.id AND tabekg_likes.user_id = ?) as liked'
            ), [$user->location_lat, $user->location_lng, $user->location_lat, $user->id]);
        });

        static::creating(function ($item) {
            $item->user_id = Auth::user()->id;
        });
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function comments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comment::class, 'post_id', 'id')->with('user');
    }
}
