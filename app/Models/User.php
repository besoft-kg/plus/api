<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $casts = [
        'provider_data' => 'array',
        'settings' => 'array',
        'last_action' => 'datetime',
    ];

    protected $with = [
        'picture',
    ];

    protected $hidden = [
        'provider',
        'provider_id',
        'provider_data',
        'created_at',
        'email',
        'updated_at',
        'language',
        'location_last_refresh',
        'location_lat',
        'location_lng',
        'settings',
    ];

    public function getIsNewAttribute(): bool
    {
        return empty($this->attributes['login']) || $this->attributes['login'] === NULL;
    }

    public function picture()
    {
        return $this->hasOne('App\Models\Image', 'id', 'picture_id');
    }
}
