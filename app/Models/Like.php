<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Like extends Model
{
  protected $fillable = [
    'user_id',
    'post_id',
  ];

  protected static function booted()
  {
    static::creating(function ($item) {
      $item->user_id = Auth::user()->id;
    });
  }

  public function post()
  {
    return $this->hasOne(Post::class, 'id', 'post_id')->with('user');
  }
}
