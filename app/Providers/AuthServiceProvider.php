<?php

namespace App\Providers;

use App\Models\User;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Firebase\Auth\Token\Exception\InvalidToken;
use App\Models\Session;
use Carbon\Carbon;
use \InvalidArgumentException;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            $header = $request->header('Authorization', '');
            $token = null;

            if (Str::startsWith($header, 'Bearer ')) {
                $token = Str::substr($header, 7);
            }

            if ($token) {
                $auth = app('firebase.auth');

                try {
                    $verifiedIdToken = $auth->verifyIdToken($token);
                } catch (InvalidToken | InvalidArgumentException $e) {
                    return null;
                }

                $session = Session::where('key', $verifiedIdToken->claims()->get('sub'))->first();

                if (!$session) return null;

                $session->last_action = Carbon::now();
                $session->save();

                $user = User::where('provider', 'phone_number')->where('provider_id', substr($verifiedIdToken->claims()->get('phone_number'), 1))->first();

                if ($user) {
                    $user->last_action = Carbon::now();
                    $user->save();
                }

                return $user;
            } else {
                return null;
            }
        });
    }
}
