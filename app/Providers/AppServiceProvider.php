<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot() {
        if (env('APP_DEBUG')) {
            DB::listen(function ($query) {
                Log::info(
                    'DB QUERY => ' . Str::replaceArray('?', $query->bindings, $query->sql)
                );
            });
        }
        date_default_timezone_set('Asia/Bishkek');
    }
}
