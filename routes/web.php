<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return ['status' => 'success', 'message' => 'Besoft Plus API is running...'];
});

$router->group([
    'namespace' => 'V1',
    'prefix' => 'v1'
], function () use ($router) {

    $router->group(['middleware' => ['auth']], function () use ($router) {

        // AUTH REQUIRED

        $router->post('/profile', 'ProfileController@post_index');
        $router->get('/profile', 'ProfileController@get_index');

        $router->get('/profile/like', 'ProfileController@get_like');

        $router->post('/profile/picture', 'ProfileController@post_picture');

        $router->get('/profile/post', 'ProfileController@get_post');

        $router->post('/post', 'PostController@post_index');
        $router->get('/post', 'PostController@get_index');

        $router->post('/post/like', 'PostController@post_like');
        $router->get('/post/like', 'PostController@get_like');

        $router->post('/like', 'PostController@post_like');
        $router->get('/like', 'PostController@get_like');

        $router->post('/post/comment', 'PostController@post_comment');
        $router->get('/post/comment', 'PostController@get_comment');

        $router->post('/comment', 'PostController@post_comment');
        $router->get('/comment', 'PostController@get_comment');

        //$router->get('/me', 'MeController@get_index');

        //        $router->get('/user', 'UserController@get_index');
        //        $router->post('/user', 'UserController@post_index');
        //        $router->delete('/user', 'UserController@delete_index');

        $router->get('/category', 'CategoryController@get_index');
        $router->post('/category', 'CategoryController@post_index');
        $router->delete('/category', 'CategoryController@delete_index');

        //        $router->post('/product', 'ProductController@post_index');
        //        $router->delete('/product', 'ProductController@delete_index');
        //
        //        $router->delete('/product/image', 'ProductController@delete_image');

        //        $router->get('/region', 'RegionController@get');
        //
        //        $router->get('/product/search', 'ProductController@get_search');
        //
        //        $router->post('/store', 'StoreController@postIndex');
        //        $router->get('/store', 'StoreController@getIndex');
        //
        //        $router->post('/offer', 'OfferController@post_index');
        //        $router->get('/offer', 'OfferController@get_index');
        //        $router->delete('/offer', 'OfferController@delete_index');
    });

    $router->post('/auth', 'AuthController@post_index');
    $router->get('/auth', function () {
        return phpinfo();
    });
});
