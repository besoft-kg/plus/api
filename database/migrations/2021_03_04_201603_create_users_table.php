<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('provider', 15);
            $table->string('provider_id', 255);
            $table->json('provider_data')->nullable();

            $table->string('login', 55)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('full_name', 255)->nullable();
            $table->text('bio')->nullable();
            $table->unsignedBigInteger('picture_id')->nullable();
            $table->timestamp('last_action')->useCurrent();
            $table->enum('language', ['ru', 'ky'])->default('ru');
            $table->json('settings')->nullable();

            $table->float('location_lat', 10, 6)->nullable();
            $table->float('location_lng', 10, 6)->nullable();
            $table->timestamp('location_last_refresh')->nullable();

            $table->foreign('picture_id')->references('id')->on('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
